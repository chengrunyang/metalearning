# Metalearning

To facilitate better metalearning we can collaboratively build a dataset
useful for metalearning. Such dataset would consist of samples of the following:
 * problem
 * dataset
 * pipeline
 * runtime arguments used
 * outcome
 * other metadata (context)

The aim of this repository is to standardize how this data is represented
so that it can be useful to all performers. Moreover, for easier sharing and
consumption of this data, some common utilities and services might be developed.

## Considerations

* Pipelines should describe also interactions by the user.
  * We should store some user identifier so one can learn across users.
* All pipelines we are collecting should be ran pipelines at least once.
  * Ideally, outcome object would be (approximately )the same if pipeline is rerun with same resources available.
* Outcome object should contain:
  * metrics on input data
  * resources used for running
    * time, memory, compute used
    * per primitive and for the whole pipeline
  * user identifier
  * did user choose the pipeline
  * a textual description why user chose the pipeline
  * failure - why it failed
    * for example, out of memory issue
  * when was it discovered:
    * during pretraining
    * during evaluation
* Same pipeline descriptions can be used also in TA2-TA3 API:
  * To describe the pipeline to the user.
  * To tell TA2 system which pipeline to build using placeholders (and TA2 system then autocompletes the pipeline).
* We should also include attribution for pipelines:
  * Which team did it, source.
  * Timestamp.
* How we standardize same cross validation and general loop of preprocessing (dataset splitting) and score computation,
  so that outcome object is useful and share same properties? If every pipeline can be anything, then outcomes will
  be hard to compare for the same dataset. But if we know that dataset splitting and score computation is done the same,
  then we can compare outcome objects.

# Database structure

Because there are one-to-many and many-to-many relations between pipeline run and problem, dataset, and pipeline,
those three are stored each in its own database collection and referenced from a pipeline run document.
So in JSON, one pipeline run could be represented as:

```yaml
{
  "id": <pipeline run ID>,
  "schema": <overall pipeline run schema version/URI>,
  "problem": {
    "$ref": <problem document ID>
  },
  "datasets": [
    {
      "$ref": <dataset document ID>
    }
  ],
  "pipeline": {
    "$ref": <pipeline ID>
  },
  <pipeline run fields>
}
```

For future compatibility, each run can reference multiple datasets.

Pipeline is a reference as well because for each pipeline multiple runs on different set of resources and hyper-parameter
configurations can be made.

In storage, we can denormalize database structure and inline referenced objects to make queries
easier, if we will use a database which does not support joins.

## Problem

We use [problem description](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/problem.json)
to describe problems. Because problem IDs are not necessary unique (there can be multiple versions with same problem ID) we
expect that "problem document ID" identifies a particular problem. Example:

```yaml
{
  "_id": <problem document ID>,
  "problem": {
    "id": "iris_problem_1",
    "version": "1.0",
    "description": "Distinguish Iris flowers of three related species.",
    "name": "Distinguish Iris flowers",
    "performance_metrics": [
      {
        "metric": "ACCURACY"
      }
    ],
    "task_subtype": "MULTICLASS",
    "task_type": "CLASSIFICATION"
  },
  "inputs": [
    {
      "dataset_id": "iris_dataset_1",
      "targets": [
        {
          "target_index": 0,
          "resource_id": "0",
          "column_index": 5,
          "column_name": "species"
        }
      ]
    }
  ],
  "outputs": {
    "predictions_file": "predictions.csv",
    "scores_file": "scores.csv"
  }
}
```

It might happen that there exist multiple equal or equivalent problem documents with different problem document IDs in the
metalearning database. This is on the user of the database to detect and resolve (using their definition of equivalence).

## Dataset

We use [dataset schema](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/container.json)
to describe datasets. Similar to problems, also dataset IDs are not unique, so a "dataset document ID" identifies a particular
dataset. Dataset document stored in the database could consist of all top-level metadata (and metafeatures) of a dataset,
including metadata necessary to retrieve a copy of a dataset. But when using common D3M datasets only few fields necessary
to identify a particular version of a dataset are required. Example for full top-level metadata:

```yaml
{
  "_id": <dataset document ID>,
  "id": "iris_dataset_1",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/container.json",
  "version": "1.0",
  "digest": "b6e903f0482c9e342112dda0961c2bc9544dfad4535d3099ab29741b34c62d55",
  "structural_type": "d3m.container.Dataset",
  "name": "Iris Dataset",
  "location_uris": [
    "https://gitlab.com/datadrivendiscovery/tests-data/raw/master/datasets/iris_dataset_1/datasetDoc.json"
  ],
  "dimension": {
    "name": "resources",
    "semantic_types": [
      "https://metadata.datadrivendiscovery.org/types/DatasetResource"
    ],
    "length": 1
  },
  "source": {
    "license": "CC",
    "redacted": false,
    "human_subjects_research": false
  }
}
```

And only required fields:

```yaml
{
  "_id": <dataset document ID>,
  "id": "iris_dataset_1",
  "version": "1.0",
  "digest": "b6e903f0482c9e342112dda0961c2bc9544dfad4535d3099ab29741b34c62d55"
}
```

It might happen that there exist multiple equal or equivalent dataset documents with different dataset document IDs in the
metalearning database. This is on the user of the database to detect and resolve (using their definition of equivalence).
In the case of datasets, one can use dataset's digest to determine which datasets share the same dataset description
and files.

## Pipeline

We use [pipeline schema](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/pipeline.json)
to describe pipeline. Pipelines have unique IDs and we can use them directly.

Equal or equivalent pipelines can happen to exist in the database. It is not required from
systems to assure that there are no duplicates. Generally, the idea is simple: if a pipeline
was generated by your system, create a new entry into the database for it, listing yourself
as an author, and store results of running it. If you find a pipeline by searching over this
database, then do not create a new entry, but just record results you got running it.

## Pipeline run fields

### Steps and their execution

During run of a pipeline, primitives receive a `hyperparams` constructor argument, and
their methods can receive also extra arguments which do not depend on pipeline data. They
might be runtime arguments or overriding hyper-parameters for a given call (arguments of
kind `RUNTIME` and `HYPERPARAMETER` in primitive's metadata, respectively). `steps` field
provides values for all of those. Moreover, each method can be called multiple times, in
an iterative fashion. This is also recorded in this field. Steps also record information
about their execution. In JSON:

```yaml
# In same order as they are listed in the pipeline. Each step
# corresponds to a step in the pipeline, order-wise.
"steps": [
  {
    # Together with hyper-parameters listed as part of a pipeline they complete all values necessary to
    # instantiate "hyperparams" constructor argument. All hyper-parameter values have to be listed explicitly,
    # even if the value matches the default value of a hyper-parameter.
    "hyperparams": {
      <map between hyper-parameter names and their values>
    },
    "random_seed": <integer>,
    # Information about non-constructor method calls, in order in which they were called.
    "method_calls": [
      {
        "name": "set_training_data"
      },
      {
        "name": "fit"
         # By not listing any arguments, we are using default value for a "threshold" hyper-parameter (the
         # non-overridden value). Runtime has to still explicitly pass the hyper-parameter's value as an
         # argument to the method call.
      },
      {
        "name": "fit",
        # Specifying "calls" allows one to combine multiple calls with same arguments into one record.
        # In this example, this means that this method has been called twice more.
        "calls": 2,
        # You can list arguments for any runtime argument which has been provided, or a hyper-parameter overridden.
        "arguments": {
          # In this example, we are overriding the "threshold" hyper-parameter for two calls.
          "threshold": <value>
        }
      },
      {
        "name": "produce",
        # If a runtime argument is not specified (like "iterations" in this case), then a default value is used.
        "arguments": {
          "timeout": <value>
        }
      }
    ],
    "status": <"SUCCESS" or "FAILURE">,
    # If step failed.
    "failure": {
      "message": <failure message>
    },
    # Python LogRecord entries during a step run.
    "logging": [
      # See: https://docs.python.org/3/library/logging.html#logging.LogRecord
      {
        "created": ...,
        "name": ...,
        "level": ...,
        "pathname": ...,
        "lineno": ...,
        "message": ...
      },
      ...
    ],
    # If this step is a sub-pipeline, then here come steps of a sub-pipeline, recursively.
    "steps": [
      ...
    ],
    "start": <step run start, absolute timestamp>,
    "end": <step run end, absolute timestamp>
  }
],
"start": <pipeline run start, absolute timestamp>,
"end": <pipeline run end, absolute timestamp>,
# Status for the whole pipeline run.
"status": <"SUCCESS" or "FAILURE">,
# Failure which prevented pipeline to run at all.
"failure": {
  "message": <failure message>
}
```

### Evaluation fields

Evaluation fields describe evaluation context and results. In JSON, the following is a sketch of its representation:

```yaml
{
  "evaluation": {
    "before": {
      "pipeline": {
        "id": <ID of the pipeline which prepares data for the evaluation>    
      },
      "random_seed": <integer>,
      // Hyper-parameter valus for free hyper-parameters of the pipeline.
      "hyperparams": {
        ...
      }
    },
    "after": {
      "pipeline": {
        "id": <ID of the pipeline which processes outputs from the evaluated pipeline to compute the score>
      },
      "random_seed": <integer>,
      // Hyper-parameter valus for free hyper-parameters of the pipeline.
      "hyperparams": {
        ...
      }
    }
  },
  "scores": [
    {
      "metric": <metric name>,
      "fold": <integer>,
      "targets": [
        {
          "target_index": 0,
          "resource_id": "0",
          "column_index": 5,
          "column_name": "species"
        }
      ],
      "value": <value>
    }
  ],
  "predictions": [
    {
      "fold": <integer>,
      "values: [
         <a list of predictions themselves, every element is a list of column values, in Lincoln Labs predictions format>  
      ]
    }
  ],
}
```

While `evaluation` describes evaluation approach used, preparation of the data and any postprocessing
should be described using standard pipelines for known evaluation approaches. Not all logic of evaluation
has to be in these pipelines, but the logic which is specific to datasets should. Ideally,
evaluation pipeline before evaluated pipeline maps input datasets to output datasets in a way that
the whole pipeline is optional if the evaluated pipeline is run outside of evaluation.
Similarly, pipeline after evaluated pipeline is used only for evaluation.

### Context fields

There are additional fields to store other metadata for a pipeline run. In JSON:

```yaml
"context": <"PRETRAINING", "TESTING", "EVALUATION", "PRODUCTION">,
"users": [
  {
    "id": <UUID for the user, if user is associated with the run>,
    "choosen": <boolean, was this run because pipeline was choosen by this user>,
    "reason": <textual reason provided by the user why the run was choosen by this user>
  }
],
# UUID is decided by whoever runs it, but it should be globally unique.
"worker_id": <UUID for the worker>,
# If reference engine was used.
"engine_version": <git commit hash of the reference engine version used>,
# Resources used for the whole run.
"resources": {
  "cpu": ...,
  "memory": ...,
  "gpu": ...,
  "disk": ...
}
```

### Pipeline description example
```python
from d3m.metadata.pipeline import Pipeline, PrimitiveStep, ArgumentType, PipelineContext
from d3m import primitives

# Denormalize -> DatasetToDataFrame -> MetafeatureExtractor

# Creating Pipeline
pipeline_description = Pipeline(context=PipelineContext.TESTING)
pipeline_description.add_input(name='inputs')

# Step 0: Denormalize
step_0 = PrimitiveStep(primitive_description=primitives.datasets.Denormalize.metadata.query())
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 1: DatasetToDataFrame
step_1 = PrimitiveStep(primitive_description=primitives.datasets.DatasetToDataFrame.metadata.query())
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_output('produce')
pipeline_description.add_step(step_1)

# Step 2: ColumnParser
step_2 = PrimitiveStep(primitive_description=primitives.data.ColumnParser.metadata.query())
step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_output('produce')
pipeline_description.add_step(step_2)

# Step 3: MetafeatureExtractor
step_3 = PrimitiveStep(primitive_description=
                            primitives.byudml.metafeature_extraction.MetafeatureExtractor.metadata.query())
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_3.add_output('produce')
pipeline_description.add_step(step_3)

# Adding output step to the pipeline
pipeline_description.add_output(name='Metafeatures', data_reference='steps.3.produce')

with open('pipeline.json', 'w') as write_file:
    write_file.write(pipeline_description.to_json(indent=4, sort_keys=True, ensure_ascii=False))
```
#### Output
```json
{
  "id": "75034a2f-7723-4257-b1f7-f64013905155",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json",
  "created": "2018-06-04T21:05:32.233121Z",
  "context": "PRODUCTION",
  "inputs": [
    {
      "name": "inputs"
    }
  ],
  "outputs": [
    {
      "data": "steps.3.produce",
      "name": "Metafeatures"
    }
  ],
  "steps": [
    {
      "type": "PRIMITIVE",
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "inputs.0"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ],
      "primitive": {
        "id": "f31f8c1f-d1c5-43e5-a4b2-2ae4a761ef2e",
        "version": "0.2.0",
        "python_path": "d3m.primitives.datasets.Denormalize",
        "name": "Denormalize datasets",
        "digest": "b668a0844e76fa390468a558303e9dd3d4f078321550ae090bd373d45b056017"
      }
    },
    {
      "type": "PRIMITIVE",
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "steps.0.produce"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ],
      "primitive": {
        "id": "4b42ce1e-9b98-4a25-b68e-fad13311eb65",
        "version": "0.2.0",
        "python_path": "d3m.primitives.datasets.DatasetToDataFrame",
        "name": "Dataset to DataFrame converter",
        "digest": "b668a0844e76fa390468a558303e9dd3d4f078321550ae090bd373d45b056017"
      }
    },
    {
      "type": "PRIMITIVE",
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "steps.1.produce"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ],
      "primitive": {
        "id": "d510cb7a-1782-4f51-b44c-58f0236e47c7",
        "version": "0.2.0",
        "python_path": "d3m.primitives.data.ColumnParser",
        "name": "Parses strings into their types",
        "digest": "b668a0844e76fa390468a558303e9dd3d4f078321550ae090bd373d45b056017"
      }
    },
    {
      "type": "PRIMITIVE",
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "steps.2.produce"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ],
      "primitive": {
        "id": "28d12214-8cb0-4ac0-8946-d31fcbcd4142",
        "version": "0.4.0",
        "python_path": "d3m.primitives.byudml.metafeature_extraction.MetafeatureExtractor",
        "name": "Dataset Metafeature Extraction",
        "digest": "5d317e40543cc5d9e8b4431514197affc07ecd7d43bc23decb21a17e41f45fd9"
      }
    }
  ]
}
```

